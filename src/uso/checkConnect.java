/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uso;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

/**
 *
 * @author arbuz
 */
public class  checkConnect {

    public String dbConnect(String db_connect_string,
            String db_userid,
            String db_password) {
        String status = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = DriverManager.getConnection(db_connect_string,
                    db_userid, db_password);
            status = "Connected";
        } catch (Exception e) {
            e.printStackTrace();
            status =  e.getMessage();
        }
        return status;
    }
}
